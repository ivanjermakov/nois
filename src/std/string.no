type String

impl String {
    fn concat(self, other: Self): Self {
        concatString(self, other)
    }
}

impl io::Display for String {
    fn fmt(self): String {
        self
    }
}

fn concatString(a: String, b: String): String
